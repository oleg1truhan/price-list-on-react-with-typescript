import React, {useEffect, useState} from 'react';

import Books from './components/Book/Books';
import BookFilter from './components/BookFilter/BookFilter';
import {IBook} from './types/book';

import './App.scss';
import Cart from './components/Cart/Cart';

function App() {

    const [booksData, setBooksData] = useState<IBook[]>([]);
    const [orderBy, setOrderBy] = useState(false);
    const [totalOrders, setTotalOrders] = useState<IBook[]>([]);
    const [totalPrice, setOrdersPrice] = useState(0);

    const filterBooks = (value: string) => {
        const filterBooks = booksData.filter((b) => b.category.toLowerCase().includes(value.toLowerCase()));
        setBooksData(filterBooks);
    };

    const sortBook = () => {
        const copyBook = booksData.slice();
        if (orderBy) {
            const sortedBookByAsc = copyBook.sort((a, b) => a.price > b.price ? 1 : -1);
            setBooksData(sortedBookByAsc);
        } else {
            const sortedBookByDesc = copyBook.sort((a, b) => a.price > b.price ? -1 : 1);
            setBooksData(sortedBookByDesc);
        }
        setOrderBy(!orderBy);
    };

    const calculatedTotalPrice = (book: IBook) => {
        if (!totalOrders.includes(book)) {
            setTotalOrders([...totalOrders, book]);
        }
    };

    useEffect(() => {
        let orderSum = totalOrders.reduce((sum, current) => sum + current.price, 0);
        setOrdersPrice(orderSum);
    }, [totalOrders]);

    return (
        <div className="wrapper">
            <main className="app">
                <section className="app__body">
                    <BookFilter orderBy={orderBy} sortBook={sortBook} filterBooks={filterBooks}/>
                    <Books calculatedTotalPrice={calculatedTotalPrice} booksData={booksData}
                           setBooksData={setBooksData}/>
                </section>
                <section>
                    <Cart totalPrice={totalPrice}/>
                </section>
            </main>
        </div>
    );
}

export default App;
