export interface IBook {
    id: number;
    price: number;
    category: string;
    name: string;
}
