import React, {FC} from 'react';
import classes from './Cart.module.scss';

interface IProps {
    totalPrice: number;
}

const Cart: FC<IProps> = ({totalPrice}) => {
    return <div className={classes.card}>
                <strong>Total Price: </strong>{totalPrice}$
            </div>;
};

export default Cart;
