import React, {ChangeEvent, FC} from 'react';

import {category} from '../../constants';
import classes from './BookFilter.module.scss';

interface Props {
    orderBy: boolean;
    filterBooks: (category: string) => void;
    sortBook: () => void;
}

const BookFilter: FC<Props> = ({orderBy, filterBooks, sortBook}) => {

    const onSelectChange = (e: ChangeEvent<HTMLSelectElement>) => {
        filterBooks(e.target.value);
    };

    const optionsRender = (categoryArray: string[]) => {
        return categoryArray.map((c: string, i: number) => <option key={i} value={c}>{c}</option>);
    };

    return <div className={classes.filter}>
        <div>
            <button className={classes.button} onClick={sortBook}>OrderBy</button>
            {orderBy ? <span>&darr;</span> : <span>&uarr;</span>}
        </div>
        <div>
            <select onChange={onSelectChange}>
                {optionsRender(category)};
            </select>
        </div>
    </div>;
};

export default BookFilter;
