import React, {FC, useEffect} from 'react';
import axios from 'axios';
import Book from './Book';
import {IBook} from '../../types/book';

interface IProps {
    booksData: IBook[],
    setBooksData: (books: IBook[]) => void
    calculatedTotalPrice: (book: IBook) => void
}

const Books: FC<IProps> = ({calculatedTotalPrice, booksData, setBooksData}) => {

    useEffect(() => {
        getData();
    }, []);

    const getData = async () => {
        try {
            const books = await axios.get<IBook[]>('./books.json');
            setBooksData(books.data);
        } catch (e) {
            console.log(e);
        }
    };

    const booksListRender = (booksArray: IBook[]) => {
        return booksArray.map((b) =>
            <Book
                key={b.id}
                book={b}
                calculatedTotalPrice={calculatedTotalPrice}
            />
        )
    }

    return <>{booksListRender(booksData)}</>;
};

export default Books;
