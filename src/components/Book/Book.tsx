import React, {FC} from 'react';

import {IBook} from '../../types/book';
import classes from './Book.module.scss';

interface IProps {
    book: IBook
    calculatedTotalPrice: (book: IBook) => void
}

const Book: FC<IProps> = ({book, calculatedTotalPrice}) => {
    return <div className={classes.book} onClick={() => calculatedTotalPrice(book)}>
        <div className={classes.body}>
            <div className={classes.column}>
                <p className={classes.number}>{book.id}.</p>
                <p className={classes.name}>{book.name}</p>
            </div>
            <div className={classes.column}>
                <p className={classes.price}>{book.price}$</p>
            </div>
        </div>
    </div>;
};

export default Book;
